package com.vitavat.mobilecodingtest

import com.vitavat.mobilecodingtest.domain.repository.DepartmentRepository
import com.vitavat.mobilecodingtest.model.response.DepartmentDetailResponse
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import io.mockk.MockKAnnotations
import io.mockk.every
import io.mockk.impl.annotations.MockK
import io.mockk.mockk
import io.reactivex.Observable
import kotlinx.coroutines.runBlocking
import org.hamcrest.CoreMatchers
import org.hamcrest.MatcherAssert
import org.junit.Before
import org.junit.Test


class DepartmentRepositoryTest {

    @MockK
    lateinit var departmentRepository: DepartmentRepository

    @Before
    fun setUp() {
        MockKAnnotations.init(this)
    }

    @Test
    fun getDepartmentData() = runBlocking {
        val departmentResponse = mockk<Observable<DepartmentResponse>>()
        every { runBlocking { departmentRepository.getListDepartment() } } returns (departmentResponse)

        val result = departmentRepository.getListDepartment()
        MatcherAssert.assertThat(
            "Received result [$result] & mocked [$departmentResponse] must be matches on each other!",
            result,
            CoreMatchers.`is`(departmentResponse)
        )
    }

    @Test
    fun getDepartmentDetailSuccess() = runBlocking {
        val departmentResponse = mockk<Observable<DepartmentDetailResponse>>()
        every { runBlocking { departmentRepository.getDepartmentDetail("1") } } returns (departmentResponse)

        val result = departmentRepository.getDepartmentDetail("1")
        MatcherAssert.assertThat(
            "Received result [$result] & mocked [$departmentResponse] must be matches on each other!",
            result,
            CoreMatchers.`is`(departmentResponse)
        )
    }
}