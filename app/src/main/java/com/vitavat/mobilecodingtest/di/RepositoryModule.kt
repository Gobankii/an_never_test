package com.vitavat.mobilecodingtest.di

import com.vitavat.mobilecodingtest.config.remote.ApiDepartmentService
import com.vitavat.mobilecodingtest.domain.repository.DepartmentRepository
import com.vitavat.mobilecodingtest.domain.repository.DepartmentRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class RepositoryModule {

    @Provides
    @Singleton
    fun provideDepartmentListNetwork(apiDepartmentService: ApiDepartmentService): DepartmentRepository {
        return DepartmentRepositoryImpl(apiDepartmentService)
    }

}
