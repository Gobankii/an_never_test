package com.vitavat.mobilecodingtest.di

import com.vitavat.mobilecodingtest.domain.repository.DepartmentRepository
import com.vitavat.mobilecodingtest.domain.usecase.DepartmentDetailUseCase
import com.vitavat.mobilecodingtest.domain.usecase.DepartmentDetailUseCaseImpl
import com.vitavat.mobilecodingtest.domain.usecase.DepartmentUseCase
import com.vitavat.mobilecodingtest.domain.usecase.DepartmentUseCaseImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UseCaseModule {

    @Singleton
    @Provides
    fun provideDepartmentUseCase(
        departmentRepository: DepartmentRepository
    ): DepartmentUseCase {
        return DepartmentUseCaseImpl(departmentRepository)
    }

    @Singleton
    @Provides
    fun provideDepartmentDetailUseCase(
        departmentRepository: DepartmentRepository
    ): DepartmentDetailUseCase {
        return DepartmentDetailUseCaseImpl(departmentRepository)
    }
}
