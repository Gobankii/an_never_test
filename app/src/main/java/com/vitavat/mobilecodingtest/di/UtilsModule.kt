package com.vitavat.mobilecodingtest.di

import android.content.Context
import com.vitavat.mobilecodingtest.common.extensions.DialogUtils
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class UtilsModule {

    @Provides
    @Singleton
    fun provideDialogUtils(@ApplicationContext context: Context): DialogUtils {
        return DialogUtils(context)
    }


}
