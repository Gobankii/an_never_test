package com.vitavat.mobilecodingtest.domain.usecase

import com.vitavat.mobilecodingtest.domain.repository.DepartmentRepository
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import io.reactivex.Observable


interface DepartmentUseCase {
    fun execute(): Observable<DepartmentResponse>
}

class DepartmentUseCaseImpl(
    private val departmentRepository: DepartmentRepository
) : DepartmentUseCase {

    override fun execute(): Observable<DepartmentResponse> {
        return departmentRepository.getListDepartment()
    }
}