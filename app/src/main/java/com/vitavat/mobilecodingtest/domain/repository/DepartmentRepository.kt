package com.vitavat.mobilecodingtest.domain.repository

import com.vitavat.mobilecodingtest.config.remote.ApiDepartmentService
import com.vitavat.mobilecodingtest.model.response.DepartmentDetailResponse
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import io.reactivex.Observable

interface DepartmentRepository {
    fun getListDepartment(): Observable<DepartmentResponse>
    fun getDepartmentDetail(id: String): Observable<DepartmentDetailResponse>
}

class DepartmentRepositoryImpl(
    private val apiDepartmentService: ApiDepartmentService
) : DepartmentRepository {

    override fun getListDepartment(): Observable<DepartmentResponse> {
        return apiDepartmentService.getListDepartment()
    }

    override fun getDepartmentDetail(id: String): Observable<DepartmentDetailResponse> {
        return apiDepartmentService.getDepartmentDetail(id = id)
    }
}