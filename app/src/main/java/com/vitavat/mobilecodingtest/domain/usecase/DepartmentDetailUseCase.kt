package com.vitavat.mobilecodingtest.domain.usecase

import com.vitavat.mobilecodingtest.domain.repository.DepartmentRepository
import com.vitavat.mobilecodingtest.model.response.DepartmentDetailResponse
import io.reactivex.Observable


interface DepartmentDetailUseCase {
    fun execute(id: String): Observable<DepartmentDetailResponse>
}

class DepartmentDetailUseCaseImpl(
    private val departmentRepository: DepartmentRepository
) : DepartmentDetailUseCase {

    override fun execute(id: String): Observable<DepartmentDetailResponse> {
        return return departmentRepository.getDepartmentDetail(id = id)
    }
}