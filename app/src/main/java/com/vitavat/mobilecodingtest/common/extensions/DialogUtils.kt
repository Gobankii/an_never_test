package com.vitavat.mobilecodingtest.common.extensions

import android.app.Dialog
import android.content.Context
import android.view.LayoutInflater
import android.view.Window
import android.view.WindowManager
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.vitavat.mobilecodingtest.R
import com.vitavat.mobilecodingtest.databinding.DialogDetailViewBinding
import dagger.hilt.android.qualifiers.ApplicationContext
import javax.inject.Inject


class DialogUtils @Inject constructor(
    @ApplicationContext val context: Context
) {
    fun dialogProductDetail(
        mTitle: String,
        mDescription: String,
        appCompatActivity: AppCompatActivity,
        mCallback: () -> Unit
    ) {
        val dialog = Dialog(appCompatActivity)
        dialog.setCanceledOnTouchOutside(false)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawableResource(android.R.color.transparent)

        val binding: DialogDetailViewBinding = DataBindingUtil.inflate(
            LayoutInflater.from(appCompatActivity),
            R.layout.dialog_detail_view,
            null,
            false
        )

        dialog.setContentView(binding.root)
        dialog.window?.setLayout(
            (context.resources.displayMetrics.widthPixels * 0.82).toInt(),
            WindowManager.LayoutParams.WRAP_CONTENT,
        )

        binding.tvTitle.text = mTitle
        binding.tvDescription.text = mDescription

        binding.tvClose.apply {
            setOnClickListener {
                mCallback.invoke()
                dialog.cancel()
            }
        }

        dialog.setCancelable(true)
        dialog.show()
    }
}