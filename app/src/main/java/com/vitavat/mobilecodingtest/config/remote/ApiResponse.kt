package com.vitavat.mobilecodingtest.config.remote

import java.net.SocketTimeoutException

object ApiResponse {

    fun onErrorResponseServer(e: Throwable): String {
        val mMessageError: String = when (e) {
            is retrofit2.HttpException -> {
                val responseBody = (e).response()!!
                if (responseBody.code() == 401) {
                    "401"
                } else if (responseBody.code() == 413) {
                    responseBody.errorBody()?.string() ?: ""
                } else if (responseBody.code() == 403) {
                    "error 403"
                } else {
                    responseBody.errorBody()?.string() ?: ""
                }
            }

            is SocketTimeoutException -> {
                "time out"
            }

            else -> {
                e.message ?: ""
            }
        }
        return mMessageError
    }
}
