package com.vitavat.mobilecodingtest.config.remote

import com.vitavat.mobilecodingtest.model.response.DepartmentDetailResponse
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import io.reactivex.Observable
import retrofit2.http.GET
import retrofit2.http.Path

interface ApiDepartmentService {

    @GET("/api/v1/departments")
    fun getListDepartment(): Observable<DepartmentResponse>


    @GET("/api/v1/departments/{id}/products")
    fun getDepartmentDetail(
        @Path("id") id: String
    ): Observable<DepartmentDetailResponse>
}