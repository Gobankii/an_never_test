package com.vitavat.mobilecodingtest.model.response


import com.google.gson.annotations.SerializedName

class DepartmentDetailResponse : ArrayList<DepartmentDetailResponse.DepartmentDetailResponseItem>(){
    data class DepartmentDetailResponseItem(
        @SerializedName("departmentId")
        val departmentId: String?,
        @SerializedName("desc")
        val desc: String?,
        @SerializedName("id")
        val id: String?,
        @SerializedName("imageUrl")
        val imageUrl: String?,
        @SerializedName("name")
        val name: String?,
        @SerializedName("price")
        val price: String?,
        @SerializedName("type")
        val type: String?
    )
}

