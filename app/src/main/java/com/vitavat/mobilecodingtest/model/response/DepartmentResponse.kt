package com.vitavat.mobilecodingtest.model.response


import com.google.gson.annotations.SerializedName

class DepartmentResponse : ArrayList<DepartmentResponse.DepartmentResponseItem>(){
    data class DepartmentResponseItem(
        @SerializedName("id")
        val id: String?,
        @SerializedName("imageUrl")
        val imageUrl: String?,
        @SerializedName("name")
        val name: String?
    )
}