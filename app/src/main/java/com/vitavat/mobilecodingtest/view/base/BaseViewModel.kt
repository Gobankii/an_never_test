package com.vitavat.mobilecodingtest.view.base

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable

interface LoadingStateViewModel {
    val loadingState: LiveData<Boolean>

    fun showLoading()
    fun hideLoading()
}

open class BaseViewModel : ViewModel(), LoadingStateViewModel {

    protected val disposable = CompositeDisposable()

    private val _loadingState = MutableLiveData<Boolean>()
    override val loadingState: LiveData<Boolean> = _loadingState

    override fun showLoading() {
        _loadingState.postValue(true)
    }

    override fun hideLoading() {
        _loadingState.postValue(false)
    }
}
