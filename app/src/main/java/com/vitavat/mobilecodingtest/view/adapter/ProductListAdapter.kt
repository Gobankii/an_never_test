package com.vitavat.mobilecodingtest.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vitavat.mobilecodingtest.databinding.ItemDepartmentCarouselBinding
import com.vitavat.mobilecodingtest.databinding.ItemProductBinding
import com.vitavat.mobilecodingtest.common.extensions.setImageView
import com.vitavat.mobilecodingtest.model.response.DepartmentDetailResponse
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import com.vitavat.mobilecodingtest.view.base.RecyclerviewAdapterDiffCallback

class ProductListAdapter :
    ListAdapter<DepartmentDetailResponse.DepartmentDetailResponseItem, ProductListAdapter.ViewHolder>(
        RecyclerviewAdapterDiffCallback()
    ) {

    var onItemClick: ((DepartmentDetailResponse.DepartmentDetailResponseItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemProductBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val articleModel = this.currentList[position]
        holder.bind(articleModel)
    }

    inner class ViewHolder(
        private val binding: ItemProductBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: DepartmentDetailResponse.DepartmentDetailResponseItem) = with(binding) {
            ivDepartment.setImageView(urlImage = data.imageUrl ?: "", isImageFitCenter = true)
            tvName.text = data.name
            tvDesc.text = data.desc
            tvPrice.text = data.price

            binding.root.setOnClickListener {
                onItemClick?.invoke(data)
            }
        }
    }
}