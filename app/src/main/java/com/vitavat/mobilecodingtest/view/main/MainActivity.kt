package com.vitavat.mobilecodingtest.view.main

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import com.vitavat.mobilecodingtest.R
import com.vitavat.mobilecodingtest.databinding.ActivityMainBinding
import com.vitavat.mobilecodingtest.view.adapter.DepartmentCarouselAdapter
import com.vitavat.mobilecodingtest.view.adapter.ProductListAdapter
import com.vitavat.mobilecodingtest.view.base.BaseActivity
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel: MainViewModel by viewModels()

    private val departmentCarouselAdapter: DepartmentCarouselAdapter by lazy { DepartmentCarouselAdapter() }

    private val productListAdapter: ProductListAdapter by lazy { ProductListAdapter() }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)

        initView()
        initViewModel()
    }

    private fun initView() {
        setViewLoading(viewModel.loadingState)
        bindDepartmentCarousel()
        bindProductList()
    }

    private fun bindProductList() {
        binding.rvProduct.apply {
            adapter = productListAdapter
        }

        productListAdapter.onItemClick = {
            dialogUtils.dialogProductDetail(
                mTitle = it.name ?: "",
                mDescription = it.desc ?: "", this
            ) {

            }
        }
    }

    private fun bindDepartmentCarousel() {
        bindTitleProductName()
        binding.rvDepartment.apply {
            adapter = departmentCarouselAdapter
        }

        departmentCarouselAdapter.onItemClick = {
            bindTitleProductName(it.name ?: "")
            viewModel.requestDepartmentDetail(id = it.id ?: "", name = it.name ?: "")
        }
    }

    private fun initViewModel() = with(viewModel) {
        requestDepartment()
        listDepartment.observe(this@MainActivity) {
            if (it.isNotEmpty()) {
                bindTitleProductName(it.first().name ?: "")
                viewModel.requestDepartmentDetail(
                    id = it.first().id ?: "",
                    name = it.first().name ?: ""
                )
            }
            departmentCarouselAdapter.submitList(it)
        }

        departmentDetail.observe(this@MainActivity) {
            bindTitleProductName(name = it.first)
            productListAdapter.submitList(it.second)
        }

        messageError.observe(this@MainActivity) {
            Toast.makeText(this@MainActivity, it, Toast.LENGTH_SHORT).show()
        }
    }

    private fun bindTitleProductName(name: String = "") {
        binding.tvTitleProductName.text = resources.getString(R.string.title_product_name, name)
    }
}