package com.vitavat.mobilecodingtest.view.base

import android.app.Dialog
import android.os.Bundle
import android.os.PersistableBundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.LiveData
import com.vitavat.mobilecodingtest.common.extensions.DialogUtils
import com.vitavat.mobilecodingtest.common.extensions.buildLoading
import javax.inject.Inject

open class BaseActivity : AppCompatActivity() {

    private var loadingDialog: Dialog? = null

    @Inject
    lateinit var dialogUtils: DialogUtils

    override fun onCreate(savedInstanceState: Bundle?, persistentState: PersistableBundle?) {
        super.onCreate(savedInstanceState, persistentState)
        loadingDialog = buildLoading()
    }

    private fun startLoading() {
        loadingDialog?.show()
    }

    private fun stopLoading() {
        loadingDialog?.dismiss()
    }

    fun setViewLoading(stateLoading: LiveData<Boolean>) {
        stateLoading.observe(this) { isLoading ->
            if (isLoading) {
                startLoading()
            } else {
                stopLoading()
            }
        }
    }
}