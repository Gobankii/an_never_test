package com.vitavat.mobilecodingtest.view.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.vitavat.mobilecodingtest.databinding.ItemDepartmentCarouselBinding
import com.vitavat.mobilecodingtest.common.extensions.setImageView
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import com.vitavat.mobilecodingtest.view.base.RecyclerviewAdapterDiffCallback

class DepartmentCarouselAdapter :
    ListAdapter<DepartmentResponse.DepartmentResponseItem, DepartmentCarouselAdapter.ViewHolder>(
        RecyclerviewAdapterDiffCallback()
    ) {

    var onItemClick: ((DepartmentResponse.DepartmentResponseItem) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val binding = ItemDepartmentCarouselBinding.inflate(
            LayoutInflater.from(parent.context),
            parent,
            false
        )
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val articleModel = this.currentList[position]
        holder.bind(articleModel)
    }

    inner class ViewHolder(
        private val binding: ItemDepartmentCarouselBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        @SuppressLint("SetTextI18n")
        fun bind(data: DepartmentResponse.DepartmentResponseItem) = with(binding) {
            ivDepartment.setImageView(urlImage = data.imageUrl ?: "", isImageFitCenter = true)
            tvTitle.text = data.name

            binding.root.setOnClickListener {
                onItemClick?.invoke(data)
            }
        }
    }
}