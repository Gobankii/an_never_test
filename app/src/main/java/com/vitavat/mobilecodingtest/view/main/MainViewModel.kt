package com.vitavat.mobilecodingtest.view.main

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.vitavat.mobilecodingtest.config.remote.ApiResponse
import com.vitavat.mobilecodingtest.domain.usecase.DepartmentDetailUseCase
import com.vitavat.mobilecodingtest.domain.usecase.DepartmentUseCase
import com.vitavat.mobilecodingtest.model.response.DepartmentDetailResponse
import com.vitavat.mobilecodingtest.model.response.DepartmentResponse
import com.vitavat.mobilecodingtest.view.base.BaseViewModel
import dagger.hilt.android.lifecycle.HiltViewModel
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val departmentUseCase: DepartmentUseCase,
    private val departmentDetailUseCase: DepartmentDetailUseCase
) : BaseViewModel() {


    private val _listDepartment = MutableLiveData<DepartmentResponse>()
    val listDepartment: LiveData<DepartmentResponse> = _listDepartment

    private val _departmentDetail =
        MutableLiveData<Pair<String, DepartmentDetailResponse?>>()
    val departmentDetail: LiveData<Pair<String, DepartmentDetailResponse?>> =
        _departmentDetail

    private val _messageError = MutableLiveData<String>()
    val messageError: LiveData<String> = _messageError

    fun requestDepartment() {
        departmentUseCase.execute().observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doFinally { hideLoading() }
            .subscribeBy(
                onNext = { response ->
                    _listDepartment.postValue(response)
                },
                onError = {
                    _messageError.postValue(ApiResponse.onErrorResponseServer(it))
                }
            )
            .addTo(disposable)
    }

    fun requestDepartmentDetail(id: String, name: String) {
        departmentDetailUseCase.execute(id = id).observeOn(AndroidSchedulers.mainThread())
            .doOnSubscribe { showLoading() }
            .doFinally { hideLoading() }
            .subscribeBy(
                onNext = { response ->
                    _departmentDetail.postValue(Pair(name, response))
                },
                onError = {
                    _departmentDetail.postValue(Pair(name, null))
                    _messageError.postValue(ApiResponse.onErrorResponseServer(it))
                }
            )
            .addTo(disposable)
    }
}